package br.com.githubapp.presentation.pull

import android.support.annotation.NonNull
import br.com.githubapp.data.repositories.repo.RepoRepository
import br.com.githubapp.domain.interactor.LoadRepoPullsUseCase
import br.com.githubapp.domain.model.RepoItem
import br.com.githubapp.domain.model.RepoOwner
import br.com.githubapp.presentation.pull.contract.PullListActivityView
import br.com.githubapp.presentation.pull.contract.PullListPresenter
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

/**
 * Created by pedrohenrique on 13/10/17.
 */
class PullListPresenterImplTest {

    lateinit var mPullListActivityView: PullListActivityView
    lateinit var mLoadRepoPullsUseCase: LoadRepoPullsUseCase
    lateinit var mPullListPresenter: PullListPresenter
    lateinit var mRepoRepository: RepoRepository

    @Before
    fun setUp() {
        mRepoRepository = mock()
        mPullListActivityView = mock()
        mLoadRepoPullsUseCase = LoadRepoPullsUseCase(mRepoRepository)
        mPullListPresenter = PullListPresenterImpl(mPullListActivityView, mLoadRepoPullsUseCase)

        val immediate = object : Scheduler() {
            override fun scheduleDirect(@NonNull run: Runnable, delay: Long, @NonNull unit: TimeUnit): Disposable {
                // this prevents StackOverflowErrors when scheduling with a delay
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }
        }

        RxJavaPlugins.setInitIoSchedulerHandler { immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediate }
    }

    @Test
    fun testLoadRepoPulls(){
        val repoOwner = RepoOwner(0, "teste", "teste")
        val repoItem = RepoItem(0, "teste", "teste", "1", "1", repoOwner)
        whenever(mLoadRepoPullsUseCase.repoRepository.loadRepoPulls(any())).thenReturn(Single.just(any()))
        mPullListPresenter.loadRepoPulls(repoItem)
        verify(mPullListActivityView).showPullList(any())

    }

}