package br.com.githubapp.presentation.home

import android.support.annotation.NonNull
import br.com.githubapp.data.repositories.repo.RepoRepository
import br.com.githubapp.domain.interactor.LoadRepositoriesUseCase
import br.com.githubapp.domain.model.Repo
import br.com.githubapp.domain.model.RepoItem
import br.com.githubapp.domain.model.RepoOwner
import br.com.githubapp.presentation.home.contract.MainActivityView
import br.com.githubapp.presentation.home.contract.RepoListPresenter
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

/**
 * Created by pedrohenrique on 13/10/17.
 */
class RepoListPresenterImplTest {

    lateinit var mMainActivityView: MainActivityView
    lateinit var mLoadRepositoriesUseCase: LoadRepositoriesUseCase
    lateinit var mRepoListPresenter: RepoListPresenter
    lateinit var mRepoRepository: RepoRepository

    @Before
    fun setUp() {
        mRepoRepository = mock()
        mMainActivityView = mock()
        mLoadRepositoriesUseCase = LoadRepositoriesUseCase(mRepoRepository)
        mRepoListPresenter = RepoListPresenterImpl(mMainActivityView, mLoadRepositoriesUseCase)

        val immediate = object : Scheduler() {
            override fun scheduleDirect(@NonNull run: Runnable, delay: Long, @NonNull unit: TimeUnit): Disposable {
                // this prevents StackOverflowErrors when scheduling with a delay
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }
        }

        RxJavaPlugins.setInitIoSchedulerHandler { immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediate }

    }

    @Test
    fun testLoadRepositoriesSuccessful(){
        val repoOwner = RepoOwner(0, "teste", "teste")
        val repoItem = RepoItem(0, "teste", "teste", "1", "1", repoOwner)
        val repoResponse = Repo(0, arrayListOf(repoItem))
        whenever(mLoadRepositoriesUseCase.repoRepository.loadRepo(any())).thenReturn(Single.just(repoResponse))
        mRepoListPresenter.loadRepositories(1)
        verify(mMainActivityView).showRepositories(any())
    }

}