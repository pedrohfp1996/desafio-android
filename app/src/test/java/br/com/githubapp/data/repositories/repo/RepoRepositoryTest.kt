package br.com.githubapp.data.repositories.repo

import android.arch.persistence.room.Room
import br.com.githubapp.BuildConfig
import br.com.githubapp.data.repositories.repo.source.RepoLocalDataSource
import br.com.githubapp.data.repositories.repo.source.RepoRemoteDataSource
import br.com.githubapp.data.room.GithubDatabase
import br.com.githubapp.domain.model.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by pedrohenrique on 13/10/17.
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(25))
class RepoRepositoryTest {
    lateinit var mRepoRepository: RepoRepository
    lateinit var mRepoRemoteDataSource: RepoRemoteDataSource
    lateinit var mRepoLocalDataSource: RepoLocalDataSource
    lateinit var mServer: MockWebServer
    lateinit var mDatabase: GithubDatabase

    @Before
    fun setUp(){

        mDatabase = Room.inMemoryDatabaseBuilder(RuntimeEnvironment.application,
                GithubDatabase::class.java).allowMainThreadQueries().build()

        mRepoLocalDataSource = RepoLocalDataSource(mDatabase)

        mServer = MockWebServer()
        mServer.start()

        val retrofit = Retrofit.Builder()
                .baseUrl(mServer.url("/").toString())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()

        mRepoRemoteDataSource = RepoRemoteDataSource(retrofit)

        mRepoRepository = RepoRepository(mRepoRemoteDataSource, mRepoLocalDataSource)
    }

    @Test
    fun testLoadRepoSuccessful(){
        val repoOwner = RepoOwner(0, "teste", "teste")
        val repoItem = RepoItem(0, "teste", "teste", "1", "1", repoOwner)
        val repoResponse = Repo(0, arrayListOf(repoItem))

        mServer.enqueue(MockResponse().setResponseCode(200).setBody(Gson().toJson(repoResponse)))

        var repo: Repo? = null
        var error: Throwable? = null

        mRepoRepository.loadRepo(1).subscribe({ t1, t2 ->
            repo = t1
            error = t2
        })

        assertNotNull(repo)
        assertNull(error)
    }

    @Test
    fun testLoadRepoFailed(){
        mServer.enqueue(MockResponse().setResponseCode(400))

        var repo: Repo? = null
        var error: Throwable? = null

        mRepoRepository.loadRepo(1).subscribe({ t1, t2 ->
            repo = t1
            error = t2
        })

        assertEquals(0, repo!!.getItems()!!.size)
        assertNull(error)
    }

    @Test
    fun testRepoPullsSuccessful(){

        val repoOwner = RepoOwner(0, "teste", "teste")
        val repoItem = RepoItem(0, "teste", "teste", "1", "1", repoOwner)

        val user = PullUser(0, "test", "test")
        val pullItemResponse = PullItem(0, "test", "test", "test", user)

        mServer.enqueue(MockResponse().setResponseCode(200).setBody(Gson().toJson(arrayListOf(pullItemResponse))))

        var pullItem: ArrayList<PullItem>? = null
        var error: Throwable? = null

        mRepoRepository.loadRepoPulls(repoItem).subscribe({ t1, t2 ->
            pullItem = t1
            error = t2
        })

        assertNotNull(pullItem)
        assertNull(error)
    }

    @Test
    fun testRepoPullsFailed(){

        val repoOwner = RepoOwner(0, "teste", "teste")
        val repoItem = RepoItem(0, "teste", "teste", "1", "1", repoOwner)

        mServer.enqueue(MockResponse().setResponseCode(400))

        var pullItem: ArrayList<PullItem>? = null
        var error: Throwable? = null

        mRepoRepository.loadRepoPulls(repoItem).subscribe({ t1, t2 ->
            pullItem = t1
            error = t2
        })

        assertEquals(0, pullItem!!.size)
        assertNull(error)
    }

    @Test
    fun testLoadRepoLocalSuccessful(){
        val repoOwner = RepoOwner(0, "teste", "teste")
        val repoItem = RepoItem(0, "teste", "teste", "1", "1", repoOwner)
        val repoResponse = Repo(0, arrayListOf(repoItem))

        mDatabase.repoDao().insertAll(repoResponse.getItems()!!)

        var repo: Repo? = null
        var error: Throwable? = null

        mRepoRepository.loadRepo().subscribe({
            t1, t2 ->

            repo = t1
            error = t2
        })

        assertNotNull(repo)
        assertNull(error)
    }

    @Test
    fun testInsertRepoSuccessful(){
        val repoOwner = RepoOwner(0, "teste", "teste")
        val repoItem = RepoItem(0, "teste", "teste", "1", "1", repoOwner)
        val repoResponse = Repo(0, arrayListOf(repoItem))

        mRepoRepository.insertRepo(repoResponse)

        var repoResult = mDatabase.repoDao().getAll()

        assertNotNull(repoResult)
        assertThat(repoResponse.getItems()!![0].id, equalTo(ArrayList(repoResult)[0].id))
    }

    @Test
    fun testRepoPullsLocalSuccessful(){
        val user = PullUser(0, "test", "test")
        val pullItemResponse = PullItem(0, "test", "test", "test", user)

        mDatabase.pullDao().insertAll(arrayListOf(pullItemResponse))

        var pullItem: ArrayList<PullItem>? = null
        var error: Throwable? = null

        mRepoRepository.loadRepoPulls().subscribe({ t1, t2 ->
            pullItem = t1
            error = t2
        })


        assertNotNull(pullItem)
        assertNull(error)

    }

    @Test
    fun testInsertPull(){
        val user = PullUser(0, "test", "test")
        val pullItemResponse = PullItem(0, "test", "test", "test", user)

        mRepoRepository.insertPull(arrayListOf(pullItemResponse))

        var pullResult = mDatabase.pullDao().getAll()

        assertNotNull(pullResult)
        assertThat(pullItemResponse.id, equalTo(ArrayList(pullResult)[0].id))
    }


}