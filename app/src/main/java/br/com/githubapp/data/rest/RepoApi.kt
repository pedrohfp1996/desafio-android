package br.com.githubapp.data.rest

import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.Repo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by pedrohenrique on 10/10/17.
 */
interface RepoApi{
    @GET("search/repositories?q=language:Java&sort=stars")
    fun loadRepositories(@Query("page") page: Int): Call<Repo>

    @GET("repos/{login}/{username}/pulls")
    fun loadRepoPulls(@Path("login") login: String, @Path("username") username: String): Call<ArrayList<PullItem>>
}