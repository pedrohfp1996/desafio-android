package br.com.githubapp.data.room.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import br.com.githubapp.domain.model.RepoItem

/**
 * Created by pedrohenrique on 11/10/17.
 */
@Dao
interface RepoItemDao{

    @Insert
    fun insertAll(repoItems: ArrayList<RepoItem>)

    @Query("SELECT * FROM RepoItem ORDER BY starsCount DESC")
    fun getAll(): List<RepoItem>

    @Query("DELETE FROM RepoItem")
    fun deleteAll()
}