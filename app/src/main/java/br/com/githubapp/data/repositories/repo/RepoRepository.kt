package br.com.githubapp.data.repositories.repo

import br.com.githubapp.data.repositories.repo.contract.RepoDataSource
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.Repo
import br.com.githubapp.domain.model.RepoItem
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

/**
 * Created by pedrohenrique on 10/10/17.
 */
class RepoRepository @Inject constructor(private val repoRemoteDataSource: RepoDataSource.RemoteDataSource,
                                         private val repoLocalDataSource: RepoDataSource.LocalDataSource): RepoDataSource.RemoteDataSource, RepoDataSource.LocalDataSource{

    override fun loadRepo(page: Int): Single<Repo> {

        val localRepo = loadRepo()
                .onErrorReturn { Repo(0, arrayListOf()) }

        val remoteRepo = repoRemoteDataSource.loadRepo(page)
                .doOnSuccess { repo -> insertRepo(repo) }
                .onErrorReturn { Repo(0, arrayListOf()) }

        return Single.zip(localRepo, remoteRepo, BiFunction { t1, t2 ->

             t2.getItems()!!.addAll(t1.getItems()!!)

             Repo(t2.getTotalCount(), t2.getItems())
        })
    }

    override fun loadRepo(): Single<Repo> = repoLocalDataSource.loadRepo()

    override fun insertRepo(repo: Repo) = repoLocalDataSource.insertRepo(repo)

    override fun loadRepoPulls(repoItem: RepoItem): Single<ArrayList<PullItem>>{
        val localPull = loadRepoPulls()
                .onErrorReturn { arrayListOf() }

        val remotePull = repoRemoteDataSource.loadRepoPulls(repoItem)
                .doOnSuccess { pulls -> insertPull(pulls) }
                .onErrorReturn { arrayListOf() }

        return Single.zip(localPull, remotePull, BiFunction { t1, t2 ->
            t2.addAll(t1)
            t2
        })
    }

    override fun loadRepoPulls(): Single<ArrayList<PullItem>> = repoLocalDataSource.loadRepoPulls()

    override fun insertPull(pullItems: ArrayList<PullItem>) = repoLocalDataSource.insertPull(pullItems)
}