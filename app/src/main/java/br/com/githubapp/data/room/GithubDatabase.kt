package br.com.githubapp.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import br.com.githubapp.data.room.dao.PullItemDao
import br.com.githubapp.data.room.dao.RepoItemDao
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.RepoItem

/**
 * Created by pedrohenrique on 11/10/17.
 */
@Database(entities = arrayOf(RepoItem::class, PullItem::class), version = 2)
abstract class GithubDatabase: RoomDatabase(){
     abstract fun repoDao(): RepoItemDao
     abstract fun pullDao(): PullItemDao
}