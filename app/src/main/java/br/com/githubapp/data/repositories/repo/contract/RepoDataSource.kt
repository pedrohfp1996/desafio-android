package br.com.githubapp.data.repositories.repo.contract

import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.Repo
import br.com.githubapp.domain.model.RepoItem
import io.reactivex.*

/**
 * Created by pedrohenrique on 10/10/17.
 */
interface RepoDataSource {

     interface LocalDataSource{
          fun insertRepo(repo: Repo)
          fun loadRepo(): Single<Repo>
          fun insertPull(pullItems: ArrayList<PullItem>)
          fun loadRepoPulls(): Single<ArrayList<PullItem>>
     }

     interface RemoteDataSource{
          fun loadRepo(page: Int): Single<Repo>
          fun loadRepoPulls(repoItem: RepoItem): Single<ArrayList<PullItem>>
     }
}