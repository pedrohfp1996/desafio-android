package br.com.githubapp.data.repositories.repo.source

import android.database.sqlite.SQLiteException
import br.com.githubapp.data.repositories.repo.contract.RepoDataSource
import br.com.githubapp.data.room.GithubDatabase
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.Repo
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by pedrohenrique on 11/10/17.
 */
class RepoLocalDataSource @Inject constructor(val database: GithubDatabase) : RepoDataSource.LocalDataSource {
    override fun loadRepo(): Single<Repo> {
        return Single.create { e ->
            try {
                val repoItems = database.repoDao().getAll()

                val repo = Repo(0, ArrayList(repoItems))

                e.onSuccess(repo)

            } catch (error: SQLiteException) {
                error.printStackTrace()
                e.onError(error)
            }
        }
    }

    override fun insertRepo(repo: Repo) {

        database.repoDao().deleteAll()

        database.repoDao().insertAll(repo.getItems()!!)

    }

    override fun loadRepoPulls(): Single<ArrayList<PullItem>> {
        return Single.create { e ->
            try{
                val pullItems = database.pullDao().getAll()

                e.onSuccess(ArrayList(pullItems))
            } catch (error: SQLiteException){
                error.printStackTrace()
                e.onError(error)
            }
        }
    }

    override fun insertPull(pullItems: ArrayList<PullItem>) {
        database.pullDao().deleteAll()

        database.pullDao().insertAll(pullItems)
    }
}