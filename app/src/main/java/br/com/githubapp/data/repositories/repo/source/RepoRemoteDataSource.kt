package br.com.githubapp.data.repositories.repo.source

import br.com.githubapp.data.repositories.repo.contract.RepoDataSource
import br.com.githubapp.data.rest.RepoApi
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.Repo
import br.com.githubapp.domain.model.RepoItem
import io.reactivex.Single
import retrofit2.HttpException
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by pedrohenrique on 10/10/17.
 */
class RepoRemoteDataSource @Inject constructor(private val retrofit: Retrofit) : RepoDataSource.RemoteDataSource {

    override fun loadRepo(page: Int): Single<Repo> {
        return Single.create { e ->
            try {
                val repoApi = retrofit.create(RepoApi::class.java)

                val callBody = repoApi.loadRepositories(page)

                val responseBody = callBody.execute()

                if(responseBody.isSuccessful){
                    e.onSuccess(responseBody.body()!!)
                }else{
                    e.onError(HttpException(responseBody))
                }

            } catch (error: Exception) {
                error.printStackTrace()
                e.onError(error)
            }
        }
    }

    override fun loadRepoPulls(repoItem: RepoItem): Single<ArrayList<PullItem>> {
        return Single.create { e ->
            try{
                val repoApi = retrofit.create(RepoApi::class.java)

                val callBody = repoApi.loadRepoPulls(repoItem.owner.login, repoItem.name)

                val responseBody = callBody.execute()

                if(responseBody.isSuccessful){
                    e.onSuccess(responseBody.body()!!)
                }else{
                    e.onError(HttpException(responseBody))
                }
            }catch (error: Exception){
                error.printStackTrace()
                e.onError(error)
            }
        }
    }

}