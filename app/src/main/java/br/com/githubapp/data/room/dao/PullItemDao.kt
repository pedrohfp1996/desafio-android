package br.com.githubapp.data.room.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.RepoItem

/**
 * Created by pedrohenrique on 12/10/17.
 */
@Dao
interface PullItemDao{
    @Insert
    fun insertAll(pullItem: ArrayList<PullItem>)

    @Query("SELECT * FROM PullItem")
    fun getAll(): List<PullItem>

    @Query("DELETE FROM PullItem")
    fun deleteAll()
}