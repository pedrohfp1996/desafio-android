package br.com.githubapp.presentation.base

/**
 * Created by pedrohenrique on 10/10/17.
 */
interface BasePresenter{
    fun finish()
}