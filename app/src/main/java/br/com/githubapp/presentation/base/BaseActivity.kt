package br.com.githubapp.presentation.base

import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import java.io.File

/**
 * Created by pedrohenrique on 10/10/17.
 */
abstract class BaseActivity<P : BasePresenter> : AppCompatActivity(), BaseView<P>