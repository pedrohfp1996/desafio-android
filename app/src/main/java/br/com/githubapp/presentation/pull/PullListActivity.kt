package br.com.githubapp.presentation.pull

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.widget.LinearLayout

import br.com.githubapp.R
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.RepoItem
import br.com.githubapp.presentation.application.GithubApplication
import br.com.githubapp.presentation.home.MainActivity
import br.com.githubapp.presentation.home.adapter.RepoListAdapter
import br.com.githubapp.presentation.pull.adapter.PullListAdapter
import br.com.githubapp.presentation.pull.contract.PullListActivityView
import br.com.githubapp.presentation.pull.contract.PullListPresenter
import br.com.githubapp.presentation.pull.di.PullListModule
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class PullListActivity : PullListActivityView() {

    //Presenter
    lateinit var mPresenter: PullListPresenter

    //Adapter
    private lateinit var mAdapter: PullListAdapter

    //Repo
    private lateinit var mRepo: RepoItem

    //Constants - OnSaveInstanceStante
    val ADAPTER_LIST = "adapterList"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_list)

        mRepo = intent.extras.getParcelable(MainActivity.REPO)

        supportActionBar!!.title = mRepo.name
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)


        //Create the presenter
        GithubApplication.appComponent
                .plusPullList(PullListModule(this))
                .inject(this)

        val layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        mAdapter = PullListAdapter(this, arrayListOf())

        mAdapter.setListener(object : PullListAdapter.OnPullItemClickListener{
            override fun onItemClick(pull: PullItem) {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(pull.url)
                startActivity(intent)
            }
        })

        recyclerView.adapter = mAdapter
        recyclerView.layoutManager = layoutManager

        mPresenter.loadRepoPulls(mRepo)

        if(savedInstanceState != null){
            mAdapter.pullList.addAll(savedInstanceState.getParcelableArrayList(ADAPTER_LIST))
        }
    }

    @Inject
    override fun setPresenter(presenter: PullListPresenter) {
        mPresenter = presenter
    }

    override fun showPullList(pullItems: ArrayList<PullItem>) {
        mAdapter.pullList.addAll(pullItems)
        mAdapter.notifyDataSetChanged()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelableArrayList(ADAPTER_LIST, mAdapter.pullList)
        super.onSaveInstanceState(outState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        mPresenter.finish()
        super.onDestroy()
    }
}
