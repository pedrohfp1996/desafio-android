package br.com.githubapp.presentation.home.contract

import br.com.githubapp.presentation.base.BasePresenter

/**
 * Created by pedrohenrique on 10/10/17.
 */
interface RepoListPresenter: BasePresenter{
    fun loadRepositories(page: Int)
}