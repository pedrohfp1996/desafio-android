package br.com.githubapp.presentation.base

/**
 * Created by pedrohenrique on 10/10/17.
 */
interface BaseView<T>{
    fun setPresenter(presenter: T)
}