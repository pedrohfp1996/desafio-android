package br.com.githubapp.presentation.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.githubapp.R
import br.com.githubapp.domain.model.RepoItem
import br.com.githubapp.presentation.utils.CircleTransform
import com.squareup.picasso.Picasso

/**
 * Created by pedrohenrique on 11/10/17.
 */
class RepoListAdapter(private val context: Context,
                      val repoList: ArrayList<RepoItem>): RecyclerView.Adapter<RepoListAdapter.ViewHolder>(){

    interface OnRepoItemClickListener{
        fun onItemClick(repoItem: RepoItem)
    }

    private lateinit var listener: OnRepoItemClickListener

    fun setListener(listener: OnRepoItemClickListener){
        this.listener = listener
    }

    override fun getItemCount(): Int = repoList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(repoList[position], context, listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.repo_item, parent, false)
        return ViewHolder(v)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItems(repo: RepoItem, context: Context, listener: OnRepoItemClickListener){
            val repositoryNameTextView = itemView.findViewById<TextView>(R.id.repositoryNameTextView)
            val descriptionTextView = itemView.findViewById<TextView>(R.id.descriptionTextView)
            val forkTextView = itemView.findViewById<TextView>(R.id.forkTextView)
            val starTextView = itemView.findViewById<TextView>(R.id.starTextView)
            val userImageView = itemView.findViewById<ImageView>(R.id.userImageView)
            val userNameTextView = itemView.findViewById<TextView>(R.id.userNameTextView)

            repositoryNameTextView.text = repo.name
            descriptionTextView.text = repo.description
            forkTextView.text = repo.forksCount
            starTextView.text = repo.starsCount
            userNameTextView.text = repo.owner.login

            Picasso.with(context)
                    .load(repo.owner.avatarUrl)
                    .transform(CircleTransform())
                    .into(userImageView)

            itemView.setOnClickListener {
                listener.onItemClick(repo)
            }
        }
    }
}