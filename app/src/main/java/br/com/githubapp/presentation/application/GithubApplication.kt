package br.com.githubapp.presentation.application

import android.app.Application
import android.support.annotation.VisibleForTesting
import br.com.githubapp.presentation.application.di.*

/**
 * Created by pedrohenrique on 10/10/17.
 */
class GithubApplication: Application(){
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = getAppComponent()
    }

    private fun getAppComponent(): AppComponent{
        return DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule("https://api.github.com/"))
                .databaseModule(DatabaseModule())
                .build()
    }

    @VisibleForTesting
    fun setComponent(component: AppComponent){
        appComponent = component
    }
}
