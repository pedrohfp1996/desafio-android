package br.com.githubapp.presentation.application.di

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by pedrohenrique on 10/10/17.
 */
@Module
class AppModule constructor(private val application: Application){
    @Provides
    @Singleton
    fun provideApplication(): Application = application
}