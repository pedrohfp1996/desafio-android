package br.com.githubapp.presentation.pull.di

import br.com.githubapp.data.repositories.repo.RepoRepository
import br.com.githubapp.data.repositories.repo.source.RepoLocalDataSource
import br.com.githubapp.data.repositories.repo.source.RepoRemoteDataSource
import br.com.githubapp.data.room.GithubDatabase
import br.com.githubapp.domain.interactor.LoadRepoPullsUseCase
import br.com.githubapp.presentation.pull.PullListPresenterImpl
import br.com.githubapp.presentation.pull.contract.PullListActivityView
import br.com.githubapp.presentation.pull.contract.PullListPresenter
import br.com.githubapp.presentation.utils.ActivityScoped
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by pedrohenrique on 12/10/17.
 */
@Module
class PullListModule constructor(private val pullListActivityView: PullListActivityView){
    @ActivityScoped
    @Provides
    fun providePullListActivityView(): PullListActivityView = pullListActivityView

    @ActivityScoped
    @Provides
    fun provideLoadRepoPullUseCase(repoRepository: RepoRepository): LoadRepoPullsUseCase
          = LoadRepoPullsUseCase(repoRepository)

    @ActivityScoped
    @Provides
    fun providePullListPresenter(presenter: PullListPresenterImpl): PullListPresenter = presenter

    @ActivityScoped
    @Provides
    fun provideRepoRepository(remote: RepoRemoteDataSource, local: RepoLocalDataSource): RepoRepository
            = RepoRepository(remote, local)

    @ActivityScoped
    @Provides
    fun provideRepoRemoteDataSource(retrofit: Retrofit): RepoRemoteDataSource =
            RepoRemoteDataSource(retrofit)

    @ActivityScoped
    @Provides
    fun provideRepoLocalDataSource(database: GithubDatabase): RepoLocalDataSource =
            RepoLocalDataSource(database)
}