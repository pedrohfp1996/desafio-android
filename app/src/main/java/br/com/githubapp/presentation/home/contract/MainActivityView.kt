package br.com.githubapp.presentation.home.contract

import br.com.githubapp.domain.model.Repo
import br.com.githubapp.presentation.base.BaseActivity

/**
 * Created by pedrohenrique on 10/10/17.
 */
abstract class MainActivityView: BaseActivity<RepoListPresenter>(){
    abstract fun showRepositories(repo: Repo)
}