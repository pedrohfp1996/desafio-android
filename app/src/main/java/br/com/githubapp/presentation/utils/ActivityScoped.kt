package br.com.githubapp.presentation.utils

import javax.inject.Scope

/**
 * Created by pedrohenrique on 10/10/17.
 */
@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScoped