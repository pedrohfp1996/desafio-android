package br.com.githubapp.presentation.pull.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.githubapp.R
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.presentation.utils.CircleTransform
import com.squareup.picasso.Picasso

/**
 * Created by pedrohenrique on 12/10/17.
 */
class PullListAdapter(private val context: Context,
                      val pullList: ArrayList<PullItem>): RecyclerView.Adapter<PullListAdapter.ViewHolder>(){

    interface OnPullItemClickListener{
        fun onItemClick(pull: PullItem)
    }

    private lateinit var listener: OnPullItemClickListener

    fun setListener(listener: OnPullItemClickListener){
        this.listener = listener
    }

    override fun getItemCount(): Int {
        return pullList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(pullList[position], context, listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.pull_item, parent, false)
        return ViewHolder(v)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bindItems(pull: PullItem, context: Context, listener: OnPullItemClickListener){
            val pullRequestNameTextView = itemView.findViewById<TextView>(R.id.pullRequestNameTextView)
            val bodyTestView = itemView.findViewById<TextView>(R.id.bodyTextView)
            val userImageView = itemView.findViewById<ImageView>(R.id.userImageView)
            val userNameTextView = itemView.findViewById<TextView>(R.id.userNameTextView)

            pullRequestNameTextView.text = pull.title
            bodyTestView.text = pull.body
            userNameTextView.text = pull.user.login

            Picasso.with(context)
                    .load(pull.user.avatarUrl)
                    .transform(CircleTransform())
                    .into(userImageView)

            itemView.setOnClickListener {
                listener.onItemClick(pull)
            }

        }
    }
}