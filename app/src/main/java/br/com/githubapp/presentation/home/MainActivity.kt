package br.com.githubapp.presentation.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.LinearLayout

import br.com.githubapp.R
import br.com.githubapp.domain.model.Repo
import br.com.githubapp.domain.model.RepoItem
import br.com.githubapp.presentation.application.GithubApplication
import br.com.githubapp.presentation.home.adapter.RepoListAdapter
import br.com.githubapp.presentation.home.contract.MainActivityView
import br.com.githubapp.presentation.home.contract.RepoListPresenter
import br.com.githubapp.presentation.home.di.RepoListModule
import br.com.githubapp.presentation.pull.PullListActivity
import br.com.githubapp.presentation.utils.EndlessRecyclerViewScrollListener
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : MainActivityView() {

    //Presenter
    private lateinit var mPresenter: RepoListPresenter

    //Adapter
    private lateinit var mAdapter: RepoListAdapter

    //ScrollListener - EndlessRecyclerViewScrollListener
    private lateinit var mScrollListener: EndlessRecyclerViewScrollListener

    //Constants - OnSaveInstanceStante
    val ADAPTER_LIST = "adapterList"

    companion object {
        val REPO = "repo"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Create the presenter
        GithubApplication.appComponent
                .plusRepoList(RepoListModule(this))
                .inject(this)

        val layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        mAdapter = RepoListAdapter(this, arrayListOf())

        mAdapter.setListener(object : RepoListAdapter.OnRepoItemClickListener{
            override fun onItemClick(repoItem: RepoItem) {
                val bundle = Bundle()
                bundle.putParcelable(REPO, repoItem)
                val intent = Intent(this@MainActivity, PullListActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        })

        recyclerView.adapter = mAdapter
        recyclerView.layoutManager = layoutManager

        mScrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                mPresenter.loadRepositories(page)
            }
        }

        recyclerView.addOnScrollListener(mScrollListener)

        mPresenter.loadRepositories(mScrollListener.currentPage)

        if(savedInstanceState != null){
            mAdapter.repoList.addAll(savedInstanceState.getParcelableArrayList(ADAPTER_LIST))
        }
    }

    @Inject
    override fun setPresenter(presenter: RepoListPresenter) {
        mPresenter = presenter
    }

    override fun showRepositories(repo: Repo) {

        if(mScrollListener.currentPage == 1){
            mAdapter.repoList.clear()
        }

        mAdapter.repoList.addAll(repo.getItems()!!)
        mAdapter.notifyDataSetChanged()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelableArrayList(ADAPTER_LIST, mAdapter.repoList)
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        mPresenter.finish()
        super.onDestroy()
    }
}
