package br.com.githubapp.presentation.application.di

import br.com.githubapp.data.repositories.repo.RepoRepository
import br.com.githubapp.data.room.GithubDatabase
import br.com.githubapp.presentation.home.di.RepoListComponent
import br.com.githubapp.presentation.home.di.RepoListModule
import br.com.githubapp.presentation.pull.di.PullListComponent
import br.com.githubapp.presentation.pull.di.PullListModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by pedrohenrique on 10/10/17.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class, DatabaseModule::class))
interface AppComponent{
    fun getDatabase(): GithubDatabase
    fun plusRepoList(module: RepoListModule): RepoListComponent
    fun plusPullList(module: PullListModule): PullListComponent
}