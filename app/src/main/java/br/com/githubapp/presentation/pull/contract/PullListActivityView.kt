package br.com.githubapp.presentation.pull.contract

import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.presentation.base.BaseActivity

/**
 * Created by pedrohenrique on 12/10/17.
 */
abstract class PullListActivityView: BaseActivity<PullListPresenter>(){
    abstract fun showPullList(pullItems: ArrayList<PullItem>)
}