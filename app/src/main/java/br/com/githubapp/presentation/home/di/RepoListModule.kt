package br.com.githubapp.presentation.home.di

import br.com.githubapp.data.repositories.repo.RepoRepository
import br.com.githubapp.data.repositories.repo.source.RepoLocalDataSource
import br.com.githubapp.data.repositories.repo.source.RepoRemoteDataSource
import br.com.githubapp.data.room.GithubDatabase
import br.com.githubapp.domain.interactor.LoadRepositoriesUseCase
import br.com.githubapp.presentation.home.RepoListPresenterImpl
import br.com.githubapp.presentation.home.contract.MainActivityView
import br.com.githubapp.presentation.home.contract.RepoListPresenter
import br.com.githubapp.presentation.utils.ActivityScoped
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by pedrohenrique on 10/10/17.
 */
@Module
class RepoListModule constructor(private val mainActivityView: MainActivityView) {
    @ActivityScoped
    @Provides
    fun provideMainActivityView(): MainActivityView = mainActivityView

    @ActivityScoped
    @Provides
    fun provideLoadRepositoriesUseCase(repository: RepoRepository): LoadRepositoriesUseCase
            = LoadRepositoriesUseCase(repository)

    @ActivityScoped
    @Provides
    fun provideRepoListPresenter(presenter: RepoListPresenterImpl): RepoListPresenter
            = presenter

    @ActivityScoped
    @Provides
    fun provideRepoRepository(remote: RepoRemoteDataSource, local: RepoLocalDataSource): RepoRepository
            = RepoRepository(remote, local)

    @ActivityScoped
    @Provides
    fun provideRepoRemoteDataSource(retrofit: Retrofit): RepoRemoteDataSource =
            RepoRemoteDataSource(retrofit)

    @ActivityScoped
    @Provides
    fun provideRepoLocalDataSource(database: GithubDatabase): RepoLocalDataSource =
            RepoLocalDataSource(database)
}