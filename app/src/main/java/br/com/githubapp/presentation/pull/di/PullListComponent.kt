package br.com.githubapp.presentation.pull.di

import br.com.githubapp.presentation.home.MainActivity
import br.com.githubapp.presentation.home.di.RepoListModule
import br.com.githubapp.presentation.pull.PullListActivity
import br.com.githubapp.presentation.utils.ActivityScoped
import dagger.Subcomponent

/**
 * Created by pedrohenrique on 12/10/17.
 */
@ActivityScoped
@Subcomponent(modules = arrayOf(PullListModule::class))
interface PullListComponent{
    fun inject(pull: PullListActivity)
}