package br.com.githubapp.presentation.application.di

import android.app.Application
import android.arch.persistence.room.Room
import br.com.githubapp.data.room.GithubDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by pedrohenrique on 12/10/17.
 */
@Module
class DatabaseModule{
    @Provides
    @Singleton
    fun provideDatabase(application: Application): GithubDatabase{
        return Room.databaseBuilder(application, GithubDatabase::class.java, "github.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}