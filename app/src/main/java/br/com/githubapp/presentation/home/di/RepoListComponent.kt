package br.com.githubapp.presentation.home.di

import br.com.githubapp.presentation.home.MainActivity
import br.com.githubapp.presentation.utils.ActivityScoped
import dagger.Subcomponent

/**
 * Created by pedrohenrique on 10/10/17.
 */
@ActivityScoped
@Subcomponent(modules = arrayOf(RepoListModule::class))
interface RepoListComponent{
    fun inject(main: MainActivity)
}