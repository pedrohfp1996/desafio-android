package br.com.githubapp.presentation.home

import br.com.githubapp.domain.interactor.LoadRepositoriesUseCase
import br.com.githubapp.domain.model.Repo
import br.com.githubapp.presentation.home.contract.MainActivityView
import br.com.githubapp.presentation.home.contract.RepoListPresenter
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by pedrohenrique on 10/10/17.
 */
class RepoListPresenterImpl @Inject constructor(private var mainActivityView: MainActivityView,
                                                private var loadRepositoriesUseCase: LoadRepositoriesUseCase): RepoListPresenter{

    init{
        mainActivityView.setPresenter(this)
    }

    override fun loadRepositories(page: Int) {
        loadRepositoriesUseCase.execute(object: DisposableSingleObserver<Repo>(){
            override fun onSuccess(repo: Repo) {
                mainActivityView.showRepositories(repo)
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }
        }, page)
    }

    override fun finish() {
        loadRepositoriesUseCase.dispose()
    }

}