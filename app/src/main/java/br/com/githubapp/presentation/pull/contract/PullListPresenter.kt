package br.com.githubapp.presentation.pull.contract

import br.com.githubapp.domain.model.RepoItem
import br.com.githubapp.presentation.base.BasePresenter

/**
 * Created by pedrohenrique on 12/10/17.
 */
interface PullListPresenter: BasePresenter{
    fun loadRepoPulls(repoItem: RepoItem)
}