package br.com.githubapp.presentation.pull

import br.com.githubapp.domain.interactor.LoadRepoPullsUseCase
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.RepoItem
import br.com.githubapp.presentation.pull.contract.PullListActivityView
import br.com.githubapp.presentation.pull.contract.PullListPresenter
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by pedrohenrique on 12/10/17.
 */
class PullListPresenterImpl @Inject constructor(private var pullListActivityView: PullListActivityView,
                                                private var loadRepoPullsUseCase: LoadRepoPullsUseCase): PullListPresenter{

    init{
        pullListActivityView.setPresenter(this)
    }

    override fun loadRepoPulls(repoItem: RepoItem) {
        loadRepoPullsUseCase.execute(object: DisposableSingleObserver<ArrayList<PullItem>>(){
            override fun onSuccess(pull: ArrayList<PullItem>) {
                pullListActivityView.showPullList(pull)
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
            }
        }, repoItem)
    }

    override fun finish() {
        loadRepoPullsUseCase.dispose()
    }
}