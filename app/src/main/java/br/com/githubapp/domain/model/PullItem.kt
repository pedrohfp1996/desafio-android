package br.com.githubapp.domain.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by pedrohenrique on 12/10/17.
 */
@Entity
class PullItem(@PrimaryKey @SerializedName("id") val id: Int,
               @SerializedName("title") val title: String,
               @SerializedName("body") val body: String,
               @SerializedName("html_url") val url: String,
               @Embedded @SerializedName("user") val user: PullUser): Parcelable{

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(PullUser::class.java.classLoader))

    override fun toString(): String =
            "PullItem(id=$id, title='$title', body='$body', url='$url', user=$user)"

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(body)
        parcel.writeString(url)
        parcel.writeParcelable(user, flags)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<PullItem> {
        override fun createFromParcel(parcel: Parcel): PullItem = PullItem(parcel)

        override fun newArray(size: Int): Array<PullItem?> = arrayOfNulls(size)
    }
}