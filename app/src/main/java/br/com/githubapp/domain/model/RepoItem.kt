package br.com.githubapp.domain.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by pedrohenrique on 10/10/17.
 */
@Entity
class RepoItem(@PrimaryKey @SerializedName("id") val id: Int,
               @SerializedName("name") val name: String,
               @SerializedName("description") val description: String,
               @SerializedName("stargazers_count") val starsCount: String,
               @SerializedName("forks_count") val forksCount: String,
               @Embedded @SerializedName("owner") val owner: RepoOwner): Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(RepoOwner::class.java.classLoader))

    override fun toString(): String {
        return "RepoItem(id=$id, " +
                "name='$name', " +
                "description='$description', " +
                "starsCount='$starsCount', " +
                "forksCount='$forksCount', " +
                "owner=$owner)"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(starsCount)
        parcel.writeString(forksCount)
        parcel.writeParcelable(owner, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RepoItem> {
        override fun createFromParcel(parcel: Parcel): RepoItem {
            return RepoItem(parcel)
        }

        override fun newArray(size: Int): Array<RepoItem?> {
            return arrayOfNulls(size)
        }
    }
}