package br.com.githubapp.domain.interactor

import br.com.githubapp.data.repositories.repo.RepoRepository
import br.com.githubapp.domain.interactor.base.SingleUseCase
import br.com.githubapp.domain.model.Repo
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by pedrohenrique on 10/10/17.
 */
class LoadRepositoriesUseCase @Inject constructor(val repoRepository: RepoRepository): SingleUseCase<Repo, Int>(){
    override fun getSingle(params: Int): Single<Repo> = repoRepository.loadRepo(params)
}