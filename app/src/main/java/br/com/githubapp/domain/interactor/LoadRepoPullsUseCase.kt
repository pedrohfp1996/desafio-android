package br.com.githubapp.domain.interactor

import br.com.githubapp.data.repositories.repo.RepoRepository
import br.com.githubapp.domain.interactor.base.SingleUseCase
import br.com.githubapp.domain.model.PullItem
import br.com.githubapp.domain.model.RepoItem
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by pedrohenrique on 12/10/17.
 */
class LoadRepoPullsUseCase @Inject constructor(val repoRepository: RepoRepository): SingleUseCase<ArrayList<PullItem>, RepoItem>(){
    override fun getSingle(params: RepoItem): Single<ArrayList<PullItem>> = repoRepository.loadRepoPulls(params)
}