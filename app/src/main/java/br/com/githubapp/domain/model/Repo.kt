package br.com.githubapp.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by pedrohenrique on 10/10/17.
 */
class Repo(@SerializedName("total_count") private val totalCount: Int?,
           @SerializedName("items") private val items: ArrayList<RepoItem>?): Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            arrayListOf<RepoItem>().apply {
                parcel.readTypedList(this, RepoItem.CREATOR)
            })

    fun getTotalCount(): Int? = totalCount

    fun getItems(): ArrayList<RepoItem>? = items

    override fun toString(): String = "Repo(totalCount=$totalCount, items=$items)"

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(items)
        parcel.writeValue(totalCount)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Repo> {
        override fun createFromParcel(parcel: Parcel): Repo = Repo(parcel)
        override fun newArray(size: Int): Array<Repo?> = arrayOfNulls(size)
    }
}