package br.com.githubapp.domain.model

import android.arch.persistence.room.ColumnInfo
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by pedrohenrique on 12/10/17.
 */
class PullUser(@ColumnInfo(name = "userId") @SerializedName("id") val id: Int,
               @SerializedName("login")val login: String,
               @SerializedName("avatar_url") val avatarUrl: String): Parcelable{

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun toString(): String {
        return "PullUser(id=$id, login='$login', avatarUrl='$avatarUrl')"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(login)
        parcel.writeString(avatarUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PullUser> {
        override fun createFromParcel(parcel: Parcel): PullUser {
            return PullUser(parcel)
        }

        override fun newArray(size: Int): Array<PullUser?> {
            return arrayOfNulls(size)
        }
    }
}