package br.com.githubapp.domain.model

import android.arch.persistence.room.ColumnInfo
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by pedrohenrique on 10/10/17.
 */
class RepoOwner(@ColumnInfo(name = "ownerId") @SerializedName("id") val id: Int,
                @SerializedName("login") val login: String,
                @SerializedName("avatar_url") val avatarUrl: String?): Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun toString(): String {
        return "RepoOwner(id=$id, login='$login', avatarUrl=$avatarUrl)"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(login)
        parcel.writeString(avatarUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RepoOwner> {
        override fun createFromParcel(parcel: Parcel): RepoOwner {
            return RepoOwner(parcel)
        }

        override fun newArray(size: Int): Array<RepoOwner?> {
            return arrayOfNulls(size)
        }
    }
}