package br.com.githubapp.presentation.application.di

import dagger.Component
import javax.inject.Singleton

/**
 * Created by pedrohenrique on 13/10/17.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkTestModule::class, DatabaseModule::class))
interface AppTestComponent: AppComponent{
}