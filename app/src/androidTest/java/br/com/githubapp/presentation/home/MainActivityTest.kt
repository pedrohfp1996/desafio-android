package br.com.githubapp.presentation.home

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import br.com.githubapp.R
import br.com.githubapp.domain.model.*
import br.com.githubapp.presentation.application.GithubApplication
import br.com.githubapp.presentation.application.di.AppModule
import br.com.githubapp.presentation.application.di.DaggerAppTestComponent
import br.com.githubapp.presentation.application.di.DatabaseModule
import br.com.githubapp.presentation.application.di.NetworkTestModule
import com.google.gson.Gson
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.net.ServerSocket

/**
 * Created by pedrohenrique on 13/10/17.
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest{

    @Rule
    @JvmField
    val mActivityRule = ActivityTestRule<MainActivity>(MainActivity::class.java, false, false)

    lateinit var mServer: MockWebServer

    @Before
    fun setUp(){
        mServer = MockWebServer()
        mServer.start()

        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val application = instrumentation.targetContext.applicationContext as GithubApplication

        val component = DaggerAppTestComponent.builder()
                .appModule(AppModule(application))
                .networkTestModule(NetworkTestModule(mServer.url("/").toString()))
                .databaseModule(DatabaseModule())
                .build()

        application.setComponent(component)

    }

    @Test
    fun testVerifyRecyclerViewDisplayed(){
        mActivityRule.launchActivity(null)

        val repoOwner = RepoOwner(0, "teste", "teste")
        val repoItem = RepoItem(0, "teste", "teste", "1", "1", repoOwner)
        val repoResponse = Repo(0, arrayListOf(repoItem))

        mServer.enqueue(MockResponse().setResponseCode(200).setBody(Gson().toJson(repoResponse)))

        onView(withId(R.id.recyclerView)).check(ViewAssertions.matches(isDisplayed()));
    }

}